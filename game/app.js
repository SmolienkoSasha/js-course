
/*
GAME RULES:
- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game
*/

function changeActivePlayer() {
    currentScore = 0;
    document.getElementById(`current-${player[activePlayer]}`).textContent = `${currentScore}`;
    activePlayer = activePlayer === 0 ? player[1] : player[0];
    document.getElementsByClassName('player-0-panel')[0].classList.toggle('active');
    document.getElementsByClassName('player-1-panel')[0].classList.toggle('active');
}

function clean () {
    document.getElementById(`score-${player[0]}`).textContent = '0';
    document.getElementById(`score-${player[1]}`).textContent = '0';
    document.getElementById(`current-${player[0]}`).textContent = '0';
    document.getElementById(`current-${player[1]}`).textContent = '0';
    document.getElementById(`name-0`).innerHTML = 'Player 1';
    document.getElementById(`name-1`).innerHTML = 'Player 2';
    document.getElementById(`name-0`).classList.remove('winner');
    document.getElementById(`name-1`).classList.remove('winner');
    document.getElementsByClassName('player-0-panel')[0].classList.remove('active');
    document.getElementsByClassName('player-1-panel')[0].classList.remove('active');
    img.style.display = 'none';
    img2.style.display = 'none';
    score = [0, 0];
    currentScore = 0;
    finishScore = 100;
}
let finishScore = document.getElementById('finishScore').value * 1;
let finishScoreElement = document.getElementById('finishScore');
finishScoreElement.onchange = function() {
    finishScore = this.value * 1;
};
let score;
let player = [0, 1];
let currentScore;
let img = document.getElementsByClassName('dice')[0];
let img2 = document.getElementsByClassName('dice')[1];
clean();
document.getElementsByClassName('player-0-panel')[0].classList.add('active');

let btnRoll = document.getElementsByClassName('btn-roll')[0];
let activePlayer = player[0];
let randomNumber;
let randomNumber2;
// let prev;

btnRoll.onclick = function () {
    if (finishScore > 10) {
        finishScoreElement.style.display = 'none';
        randomNumber =  Math.floor(Math.random() * 6) +1;
        randomNumber2 =  Math.floor(Math.random() * 6) +1;
        img.style.display = 'block';
        img2.style.display = 'block';
        img.src = `dice-${randomNumber}.png`;
        img2.src = `dice-${randomNumber2}.png`;
        if (randomNumber !== 1 && randomNumber2 !== 1) {
            currentScore += randomNumber + randomNumber2;
            document.getElementById(`current-${player[activePlayer]}`).textContent = `${currentScore}`;
            if (randomNumber === randomNumber2 && randomNumber === 6) {
                score[activePlayer] = 0;
                document.getElementById(`score-${player[activePlayer]}`).textContent = `${score[activePlayer]}`;
                changeActivePlayer();
                // prev = 0;
            }
            // prev = randomNumber;
        } else {
            changeActivePlayer();
            // prev = 0;
        }

    } else {
        alert('Sorry too small number, try for example 20');
    }
};

let holdButton = document.getElementsByClassName('btn-hold')[0];
holdButton.onclick = function () {
    if (img.style.display !== 'none' && img2.style.display !== 'none') {
        score[activePlayer] += currentScore;
        document.getElementById(`score-${player[activePlayer]}`).textContent = `${score[activePlayer]}`;
        if (score[activePlayer] < finishScore) {
            changeActivePlayer();
        } else {
            clean();
            // document.getElementsByClassName(`player-${player[activePlayer]}-panel`)[0].classList.toggle('active');
            document.getElementById(`name-${player[activePlayer]}`).innerHTML = 'Winner';
            document.getElementById(`name-${player[activePlayer]}`).classList.add('winner');
            this.style.display = 'none';
            btnRoll.style.display = 'none';
        }
    }
};


let newGame = document.getElementsByClassName('btn-new')[0];
newGame.onclick = function () {
    btnRoll.style.display = 'block';
    holdButton.style.display = 'block';
    finishScoreElement.style.display = 'block';
    finishScoreElement.value = '100';
    clean();
    document.getElementsByClassName('player-0-panel')[0].classList.add('active');
};




