import axios from 'axios';
import { key, proxy } from '../config';

export default class Recipe {
    constructor(id) {
        this.id = id;
    }

    async getRecipe() {
        try {
            const res = await axios(`${proxy}https://api.spoonacular.com/recipes/${this.id}/information?includeNutrition=false&apiKey=${key}`);
            this.title = res.data.title;
            this.img = res.data.image;
            this.auothor = res.data.creditsText;
            this.ingredients = res.data.extendedIngredients;
        } catch (e) {
            console.log(e.message());
        }
    }

    calcTime() {
        //Assuming that we need 15 min for each 3 ingredients;
        const numIng = this.ingredients.length;
        const periods = Math.ceil(numIng / 3);
        this.time = periods * 15;
    }

    calcServings() {
        this.servings = 4;
    }

    parseIngredients() {
        const unitsLong = [ 'tablespoons', 'tablespoon', 'cups', 'slices', 'teaspoons' ,'teaspoon', 'pounds' ];
        const unitsShort = [ 'tbsp', 'tbsp', 'cup', 'slice', 'ts', 'ts', 'pound' ];

        const newIngredints = this.ingredients.map( el => {
            //1) uniform units
            unitsLong.forEach( (unit, index) => {
                el.unit = el.unit.replace(unit, unitsShort[index]);
            });

            return el
        });
        this.ingredients = newIngredints;
    }

    updateServings(type) {
        const newServings = type === 'dec' ? this.servings - 1 : this.servings + 1;


        this.ingredients.forEach( cur => {
            cur.amount *= (newServings / this.servings);
        });

        this.servings = newServings;

    }
}