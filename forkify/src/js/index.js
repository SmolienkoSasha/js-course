import Search from './modules/Search';
import List from "./modules/List";
import Recipe from "./modules/Recipe";
import Likes from "./modules/Likes";
import * as searchView from './views/searchView';
import * as recipeView from './views/recipeView';
import * as listView from './views/listView';
import * as likesView from './views/likesView';
import { elements, renderLoader, clearLoader } from "./views/base";

//**Global state of the map
// --search object
//--current results object
//--shopping list object
//--liked recipes
const state = {};

//SEARCH CONTROLLER
const controlSearch = async () => {
    //1. Get query from the view
    const query = searchView.getInput();

    if (query) {
        //2. create new search object and add to state
        state.search = new Search(query);

        //3. prepare UI for results
        searchView.clearInput();
        searchView.clearPreviousResults();
        renderLoader(elements.searchRes);

        try {
            //4. search for results
            await state.search.getResult();

            //5. render results on UI
            clearLoader();
            if (state.search.result.length > 0) {
                searchView.renderResults(state.search.result);
            } else {
                alert('Sorry we did not find anything by your request, try something else');
            }
        } catch (e) {
            alert('WRONG');
            clearLoader();
        }
    }

};

elements.searchForm.onsubmit = e => {
    e.preventDefault();
    controlSearch();
};


elements.searchResPages.onclick = e => {
    const btn = e.target.closest('.btn-inline');
    if (btn) {
        const goToPage = +btn.dataset.goto;
        searchView.clearPreviousResults();
        searchView.renderResults(state.search.result, goToPage);
    }
};


//RECIPE CONTROLLER
const controlRecipe = async () => {
    //get ID from url
    const id = window.location.hash.replace('#', '');

    if (id) {
        //prepare UI for changes
        recipeView.clearRecipe();
        renderLoader(elements.recipe);

        if (state.search) searchView.highLightSelected(id);

        //create new recipe object
        state.recipe = new Recipe(id);

        try {
            //get recipe
            await state.recipe.getRecipe();
            state.recipe.parseIngredients();

            //calculate serving and time
            state.recipe.calcTime();
            state.recipe.calcServings();

            //render recipe
            clearLoader();
            recipeView.renderRecipe(state.recipe, state.likes.isLiked(id));
        } catch (e) {
            alert('Error processing recipe');
            console.log(e.message());
        }
    }
};


// window.onhashchange = controlRecipe;
// window.onload = controlRecipe;
['hashchange', 'load'].forEach( event => window.addEventListener(event, controlRecipe));


//List controller
const controlList = () => {
    //create a new list if there in none yet
    if (!state.list) state.list = new List();

    //add each ingredients to the list and UI
    state.recipe.ingredients.forEach( cur => {
        const item = state.list.addItem(cur.amount, cur.unit, cur.name);
        listView.renderItem(item);
    })
};


//handle delete and update list item events
elements.shopping.onclick = e => {
    const id = e.target.closest('.shopping__item').dataset.itemid;

    //handle the delete button
    if (e.target.matches('.shopping__delete, .shopping__delete *')) {
        //delete from state
        state.list.deleteItem(id);

        //delete from UI
        listView.deleteItem(id);
    } else if (e.target.matches('.shopping__count-value')) {
        const val = parseFloat(e.target.value, 10);
        state.list.updateCount(id, val);
    }
};


//like controller
const controlLike = () => {
    if (!state.likes) state.likes = new Likes();
    const currentID = state.recipe.id;

    //user has NOT yet liked current recipe
    if (!state.likes.isLiked(currentID)) {
        const newLike = state.likes.addLike(
            currentID,
            state.recipe.title,
            state.recipe.auothor,
            state.recipe.img
        );
        //toggle the like button
        likesView.toggleLikeBtn(true);

        //add like to UI list
        likesView.renderLike(newLike);

    //user HAS liked current recipe
    } else {
        //remove like from the state
        state.likes.deleteItem(currentID);

        //toggle the like button
        likesView.toggleLikeBtn(false);

        //remove like from UI list
        likesView.deleteLike(currentID);

    }

    likesView.toggleLikeMenu(state.likes.getNumberLikes());
};


//restore like recipes on page load
window.onload = () => {
    //testing
    state.likes = new Likes();

    //restore likes
    state.likes.readStorage();

    //toggle btn
    likesView.toggleLikeMenu(state.likes.getNumberLikes());

    //render the existing likes
    state.likes.likes.forEach(like => likesView.renderLike(like));
};



//handling recipe button clicks
elements.recipe.onclick = e => {
    if (e.target.matches('.btn-decrease, .btn-decrease *')) {
        if (state.recipe.servings > 1) {
            //decrease btn is clicked
            state.recipe.updateServings('dec');
            recipeView.updateServingsIngredients(state.recipe);
        }
    } else if (e.target.matches('.btn-increase, .btn-increase *')) {
        //increase btn is clicked
        state.recipe.updateServings('inc');
        recipeView.updateServingsIngredients(state.recipe);
    } else if (e.target.matches('.recipe__btn--add, .recipe__btn--add *')) {
        //add ingredients to shopping list
        controlList();
    } else if (e.target.matches('.recipe__love, .recipe__love *')) {
        controlLike();
    }
};



