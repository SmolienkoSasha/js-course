let budgetController = (function () {
    let Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
        this.percentage = -1;
    };

    Expense.prototype.calcPercentage = function(totalIncome) {
        // if (totalIncome > 0) {
        //     this.percentage = Math.round((this.value / totalIncome) * 100);
        // } else {
        //     this.percentage = -1;
        // }

        this.percentage = totalIncome > 0 ? Math.round((this.value / totalIncome) * 100) : -1;
    };

    Expense.prototype.getPercentage = function() {
        return this.percentage;
    };

    let Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    let calculateTotal = function(type) {
        let result = 0;

        // data.allItems[type].forEach(function (cur) {
        //     result += cur.value;
        // });

        for (let i = 0; i < data.allItems[type].length; i++) {
            result +=  data.allItems[type][i].value;
        }
        data.totals[type] = result;
    };

    let data = {
        allItems: {
            inc: [],
            exp: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1
    };


    return {
        addItem: function (type, desc, val) {
            let ID;
            //creation of new id
            if(data.allItems[type].length > 0) {
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            } else {
                ID = 0;
            }

            //creation new item 'exp' or 'inc'
            let newItem = type === 'exp' ? new Expense(ID, desc, val): new Income(ID, desc, val);
            //pushing new item to data
            data.allItems[type].push(newItem);
            //return new item
            return newItem;
        },

        deleteItem: function(type, id) {
            // 91 episode
            let ids, index;

            ids = data.allItems[type].map(function (current) {
                return current.id
            });
            index = ids.indexOf(id);

            if (index !== -1) {
                data.allItems[type].splice(index, 1);
            }
        },

        calculateBudget: function () {
            //calculate total income and expenses
            calculateTotal('exp');
            calculateTotal('inc');

            //calculate budget
            data.budget = data.totals.inc - data.totals.exp;

            //calculate percentage of income that we spent
            if (data.totals.inc > 0) {
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
            } else {
                data.percentage = -1;
            }
        },

        calculatePercentages: function() {
            data.allItems.exp.forEach(function (cur) {
                cur.calcPercentage(data.totals.inc);
            });
        },

        getPercentages: function() {
            let allPercentages = data.allItems.exp.map(function (cur) {
                return cur.getPercentage();
            });
            return allPercentages;
        },

        getBudget: function() {
            return {
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            }
        },

        testing: function () {
            console.log(data);
        }
    };
})();



let UIController = (function () {
    let DOMStrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expensesContainer: '.expenses__list',
        budgetLabel: ".budget__value",
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container',
        expensesPercLabel: '.item__percentage',
        currentMonth: '.budget__title--month'
    };

    let formatNumber = function(num, type) {
        let numSplit, int, float, sign;
        num = Math.abs(num).toFixed(2);
        numSplit = num.split('.');
        int = numSplit[0];
        if (int.length > 3) {
            int = int.substr(0, int.length -3) + ',' + int.substr(int.length -3, int.length);
        }
        float = numSplit[1];
        sign = type === 'exp' ? '-' : '+';

        return sign + ' ' + int + '.' + float;
    };

    let displayMonth = function () {
        let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November','December'];
        let month = new Date().getMonth();
        let year = new Date().getFullYear();
        document.querySelector(DOMStrings.currentMonth).innerHTML = months[month] + ' ' + year;
    };

    return {
        getInput: function () {
            return {
                type: document.querySelector(DOMStrings.inputType).value,
                description: document.querySelector(DOMStrings.inputDescription).value,
                value: +document.querySelector(DOMStrings.inputValue).value
            }
        },


        clearFields: function () {
            document.querySelector(DOMStrings.inputDescription).value = '';
            document.querySelector(DOMStrings.inputValue).value = '';
            document.querySelector(DOMStrings.inputDescription).focus();
        },

        addListItem: function(obj, type) {
            let html, newHtml, element;

            if (type === 'inc') {
                element = DOMStrings.incomeContainer;
                html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div> <div class="right clearfix"><div class="item__value">%value%</div> <div class="item__delete"> <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div> </div>';
            } else if (type === 'exp') {
                element = DOMStrings.expensesContainer;
                html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
            }

            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', formatNumber(obj.value, type));

            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },

        deleteListItem: function(selectorID) {
            let el =  document.getElementById(selectorID);
            el.parentNode.removeChild(el);
        },

        getDOMStrings: function () {
            return DOMStrings;
        },

        displayBudget: function (budget) {
            let type = budget.budget > 0 ? 'inc' : 'exp';
            document.querySelector(DOMStrings.budgetLabel).innerHTML = formatNumber(budget.budget, type);
            document.querySelector(DOMStrings.incomeLabel).innerHTML = formatNumber(budget.totalInc, 'inc');
            document.querySelector(DOMStrings.expensesLabel).innerHTML = formatNumber(budget.totalExp, 'exp');
            document.querySelector(DOMStrings.percentageLabel).innerHTML = budget.percentage > 0 ? budget.percentage + '%' : '0%';
        },

        init: function () {
            document.querySelector(DOMStrings.budgetLabel).innerHTML = '0';
            document.querySelector(DOMStrings.incomeLabel).innerHTML = '0';
            document.querySelector(DOMStrings.expensesLabel).innerHTML = '0';
            // document.querySelector(DOMStrings.budgetIncomePercentage).innerHTML = '0';
            document.querySelector(DOMStrings.percentageLabel).innerHTML = '0%';
            displayMonth();
        },

        displayPercentages: function (percentages) {
            let fields = document.querySelectorAll(DOMStrings.expensesPercLabel);
            let nodeListForEach = function(list, callback){
                for (let i = 0; i < list.length; i++) {
                    callback(list[i], i);
                }
            };

            nodeListForEach(fields, function (current, index) {
                current.textContent = percentages[index] > 0 ? percentages[index] + '%': '---';

            });
        },

        changedType: function () {
            document.querySelector(DOMStrings.inputBtn).classList.toggle('red');
            document.querySelector(DOMStrings.inputType).classList.toggle('red-focus');
            document.querySelector(DOMStrings.inputDescription).classList.toggle('red-focus');
            document.querySelector(DOMStrings.inputValue).classList.toggle('red-focus');
        }
    }

})();



let controller = (function (budgetCtrl, UICtrl) {

    let setupEventListeners = function(){
        let DOM = UICtrl.getDOMStrings();
        document.querySelector(DOM.inputBtn).onclick = ctrlAddItem;
        document.onkeypress = function (event) {
            if(event.keyCode === 13 || event.which === 13) {
                ctrlAddItem();
            }
        };

        document.querySelector(DOM.container).onclick = ctrlDeleteItem;
        document.querySelector(DOM.inputType).onchange = UICtrl.changedType;
    };


    let updateBudget = function() {
        //calculate the budget
        budgetCtrl.calculateBudget();

        //return the budget
        let budget = budgetCtrl.getBudget();

        //display the budget on the UI
        UICtrl.displayBudget(budget);
    };

    let updatePercentages = function() {
        // calculate percentages
        budgetCtrl.calculatePercentages();

        // read percentage from the budget controller
        let percentages =  budgetCtrl.getPercentages();

        //update UI
        UICtrl.displayPercentages(percentages);
        // console.log(percentages);
    };

    let ctrlAddItem = function() {
        //get the filed input data
        let input = UICtrl.getInput();

        //add the item to the budget controller

        if (input.description !== '' && !isNaN(input.value) && input.value > 0) {
            //add the item to the budget controller
            let newItem = budgetController.addItem(input.type, input.description, input.value);

            //add the item to the UI
            UICtrl.addListItem(newItem, input.type);

            //clear fields
            UICtrl.clearFields();

            //calculate and update the budget
            updateBudget();

            //update percentages
            updatePercentages();
        } else {
            alert('Fuck off pidor!!!');
        }

    };

    let ctrlDeleteItem = function (event) {
        let splitID, type, ID;
        let itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
        // console.log(itemID);

        if (itemID) {
            splitID = itemID.split('-');
            type = splitID[0];
            ID = +splitID[1];
            //delete the item from the data structure
            budgetCtrl.deleteItem(type, ID);

            //delete the item from UI
            UICtrl.deleteListItem(itemID);

            //update and show the new budget
            updateBudget();

            //update percentages
            updatePercentages();
        }
    };


    return {
        init: function () {
            UICtrl.init();
            setupEventListeners();
        }
    }


})(budgetController, UIController);

controller.init();